import Link from "next/link";
import Image from "next/image";

const Navigation = () => {
  return (
    <nav className="bg-white border-gray-200 px-2 sm:px-4 py-2.5 shadow-lg">
      <div className="container flex flex-wrap items-center justify-between mx-auto">
        <Link href="/">
          <div className="flex items-center cursor-pointer">
            <Image src="/logo.svg" alt="App Logo" width={35} height={35} />
            <span className="text-xl font-semibold text-black whitespace-nowrap">
              MPM Tools
            </span>
          </div>
        </Link>
        <div className="w-full md:block md:w-auto">
          <ul className="flex flex-col p-4 mt-4 border-gray-100 bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-slate-200">
            <li className="text-black">
              <Link href="/submit">
                <a className="block py-2 pl-3 pr-4 md:hover:underline">
                  Submit
                </a>
              </Link>
            </li>
            <li>
              <Link href="/queue">
                <a className="block py-2 pl-3 pr-4 md:hover:underline">Queue</a>
              </Link>
            </li>
            <li>
              <Link href="/quickrun">
                <a className="block py-2 pl-3 pr-4 md:hover:underline">
                  Quick Run
                </a>
              </Link>
            </li>
            <li>
              <Link href="/request">
                <a className="block py-2 pl-3 pr-4 md:hover:underline">
                  Request
                </a>
              </Link>
            </li>
            <li>
              <Link href="login">
                <a className="block py-2 pl-3 pr-4 md:hover:underline">Login</a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
