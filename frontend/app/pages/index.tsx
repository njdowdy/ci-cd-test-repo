import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>MPM Tools</title>
        <meta
          name="The digital dashboard for the Collections & Research departments at the Milwaukee Public Museum."
          content="MPM.tools!"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="h-screen pt-5 bg-gradient-to-b from-teal-600 via-blue-500 to-purple-600">
        <div className="container px-20 py-5 mx-auto text-center rounded shadow-xl bg-slate-100">
          <h1 className="text-xl font-bold">Hello, World!</h1>
          <p className="mt-5 text-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti
            nihil harum dicta vero, vel ipsa sit reprehenderit. Eos, cum itaque?
            Nihil dolorem et, maiores sunt ab quasi minima facere ex, rerum rem
            porro eaque natus. Rem laborum, unde vitae sequi, iusto ipsam eaque
            eveniet vero saepe in voluptates placeat qui laudantium harum nobis
            ea porro mollitia, omnis incidunt excepturi tenetur eum beatae
            aliquid totam. Illo, nihil! Quam unde harum possimus aliquid ratione
            quae voluptate incidunt cumque libero facilis, animi esse laudantium
            expedita voluptatem vel dolorem? Voluptatum odio ab quod atque alias
            fuga commodi provident. Sunt mollitia eligendi corrupti fuga
            consectetur!
          </p>
          <Link href="quickrun">
            <button className="px-4 py-2 mt-5 text-sm font-bold text-white bg-green-700 rounded hover:bg-green-800">
              Get Started
            </button>
          </Link>
        </div>
      </main>
    </div>
  );
};

export default Home;
