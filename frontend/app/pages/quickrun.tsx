import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

const QuickRun: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Quick Run</title>
        <meta name="Quick Run" content="Quick Run" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="h-screen pt-5 bg-gradient-to-b from-teal-600 via-blue-500 to-purple-600">
        <div className="container px-20 py-5 mx-auto mt-5 text-center rounded shadow-xl bg-slate-100">
          <h1 className="text-xl font-bold">Quick Run</h1>
          <Link href="#">
            <button className="px-4 py-2 mt-5 text-sm font-bold text-white bg-orange-500 rounded hover:bg-orange-600">
              Run
            </button>
          </Link>
        </div>
      </main>
    </div>
  );
};

export default QuickRun;
