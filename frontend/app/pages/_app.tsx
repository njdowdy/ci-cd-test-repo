import "../styles/index.css";
import type { AppProps } from "next/app";
import Navigation from "../components/Navigation";

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Navigation></Navigation>
      <Component {...pageProps} />
    </>
  );
}

export default App;
