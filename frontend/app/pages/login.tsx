import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

const Login: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Login</title>
        <meta name="Login" content="Login" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="h-screen pt-5 bg-gradient-to-b from-teal-600 via-blue-500 to-purple-600">
        <div className="container px-20 py-5 mx-auto mt-5 text-left rounded shadow-xl bg-slate-100">
          <h1 className="text-xl font-bold text-center">Login Here</h1>
          <form action="#" method="post">
            <div className="mt-6 mb-6">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-600"
              >
                Email address
              </label>
              <input
                type="email"
                id="email"
                className="bg-gray-50 border border-gray-300 text-gray-400 text-sm rounded-lg focus:ring-gray-900 focus:border-gray-900 focus:text-gray-900 block w-full p-2.5"
                placeholder="john.doe@company.com"
                required
              />
            </div>
            <div className="mb-6">
              <label
                htmlFor="password"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-600"
              >
                Password
              </label>
              <input
                type="password"
                id="password"
                className="bg-gray-50 border border-gray-300 text-gray-400 text-sm rounded-lg focus:ring-gray-900 focus:border-gray-900 focus:text-gray-900 block w-full p-2.5"
                placeholder="•••••••••"
                required
              />
            </div>
            <div className="mb-6">
              <label
                htmlFor="confirm_password"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-600"
              >
                Confirm password
              </label>
              <input
                type="password"
                id="confirm_password"
                className="bg-gray-50 border border-gray-300 text-gray-400 text-sm rounded-lg focus:ring-gray-900 focus:border-gray-900 focus:text-gray-900 block w-full p-2.5"
                placeholder="•••••••••"
                required
              />
            </div>
            <div className="flex items-start mb-6">
              <div className="flex items-center h-5">
                <input
                  id="remember"
                  type="checkbox"
                  value=""
                  className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 hover:cursor-pointer"
                  required
                />
              </div>
              <label
                htmlFor="remember"
                className="ml-2 text-sm font-medium text-gray-900"
              >
                I agree with the conditions.
              </label>
            </div>
            <div className="container flex mx-auto center-items">
              <button
                type="submit"
                className="mt-5 text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5"
              >
                Login
              </button>
              <Link href="#">
                <button className="ml-3 mt-5 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5">
                  Register
                </button>
              </Link>
            </div>
          </form>
        </div>
      </main>
    </div>
  );
};

export default Login;
