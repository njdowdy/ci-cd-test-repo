import type { NextPage } from "next";
import Head from "next/head";

const Queue: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Queue</title>
        <meta name="Queue" content="Queue" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="h-screen pt-5 bg-gradient-to-b from-teal-600 via-blue-500 to-purple-600">
        <div className="container px-20 py-5 mx-auto mt-5 text-center rounded shadow-xl bg-slate-100">
          <h1 className="text-xl font-bold">Queue</h1>
          <table></table>
        </div>
      </main>
    </div>
  );
};

export default Queue;
